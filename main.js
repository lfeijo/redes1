'use strict';

var defaults = require("./defaults");
var Internet = require("./internet");
var auxFuncs = require("./aux");

for (var property in auxFuncs) {
    if (auxFuncs.hasOwnProperty(property)) {
        global[property] = auxFuncs[property];
    }
}

// #NODE
// <node_name>,<MAC>,<IP/prefix>,<gateway>
// #ROUTER
// <router_name>,<num_ports>,<MAC0>,<IP0/prefix>,<MAC1>,<IP1/prefix>,<MAC2>,<IP2/prefix> …
// #ROUTERTABLE
// <router_name>,<net_dest/prefix>,<nexthop>,<port>

// #NODE
// n1,00:00:00:00:00:01,192.168.0.2/24,192.168.0.1
// n2,00:00:00:00:00:02,192.168.0.3/24,192.168.0.1
// n3,00:00:00:00:00:03,192.168.1.2/24,192.168.1.1
// n4,00:00:00:00:00:04,192.168.1.3/24,192.168.1.1
// #ROUTER
// r1,2,00:00:00:00:00:05,192.168.0.1/24,00:00:00:00:00:06,192.168.1.1/24
// #ROUTERTABLE
// r1,192.168.0.0/24,0.0.0.0,0
// r1,192.168.1.0/24,0.0.0.0,1

// Pacotes ARP Request: ARP_REQUEST|<MAC_src>,<MAC_dst>|<IP_src>,<IP_dst>
// Pacotes ARP Reply: ARP_REPLY|<MAC_src>,<MAC_dst>|<IP_src>,<IP_dst>
// Pacotes ICMP Echo Request: ICMP_ECHOREQUEST|<MAC_src>,<MAC_dst>|<IP_src>,<IP_dst>|<TTL>
// Pacotes ICMP Echo Reply: ICMP_ECHOREPLY|<MAC_src>,<MAC_dst>|<IP_src>,<IP_dst>|<TTL>
// Pacotes ICMP Time Exceeded: ICMP_TIMEEXCEEDED|<MAC_src>,<MAC_dst>|<IP_src>,<IP_dst>|<TTL>

var internet = new Internet();

var fileName = process.argv[2];
var command = process.argv[3];
var arg1 = process.argv[4];
var arg2 = process.argv[5];

var state = "";

var fs = require('fs');
var array = fs.readFileSync(fileName).toString().split("\n");
for(i in array) {
    var line = array[i];
    if (line) {
		if (line[0]==="#") {
			state = line;
		} else {
			var parts = line.split(",");
			if (state==="#NODE") {
				var subpart = parts[2].split("/");
				internet.addNode(parts[0], subpart[0], prefixToMask(subpart[1]), parts[3], parts[1]);
			} else if (state==="#ROUTER") {
				var interfaces = [];
				for (var i = 2; i < 2+(2*parts[1]); i+=2) {
						var subpart = parts[i+1].split("/");
						interfaces.push({
							ip: subpart[0],
							mask: prefixToMask(subpart[1]),
							mac: parts[i]
						});
				}
				internet.addRouter(parts[0], interfaces);
			} else if (state==="#ROUTERTABLE") {
				var subpart = parts[1].split("/");
				internet.dns(parts[0]).addTableEntry(subpart[0], prefixToMask(subpart[1]), parts[2], parts[3]);
			}
		}
    }
}

internet.dns(arg1)[command](internet.dns(arg2).ip);

// internet.addNode("n1", "192.168.0.2", prefixToMask("24"), "192.168.0.1", "00:00:00:00:00:01");
// internet.addNode("n2", "192.168.0.3", prefixToMask("24"), "192.168.0.1", "00:00:00:00:00:02");

// internet.addRouter("r1", [
// 	{
// 		ip: "192.168.0.1",
// 		mask: prefixToMask("24"),
// 		mac: "00:00:00:00:00:05"
// 	},
// 	{
// 		ip: "192.168.1.1",
// 		mask: prefixToMask("24"),
// 		mac: "00:00:00:00:00:06"
// 	}
// ]);

// internet.addNode("n3", "192.168.1.2", prefixToMask("24"), "192.168.1.1", "00:00:00:00:00:03");
// internet.addNode("n4", "192.168.1.3", prefixToMask("24"), "192.168.1.1", "00:00:00:00:00:04");

// internet.dns("r1").addTableEntry("192.168.0.0", prefixToMask("24"), "0.0.0.0", "0");
// internet.dns("r1").addTableEntry("192.168.1.0", prefixToMask("24"), "0.0.0.0", "1");

// internet.dns("n1").sendIpv4({
// 	protocol: "ICMP",
// 	sourceIp: internet.dns("n1").ip,
// 	destinationIp: internet.dns("n2").ip,
// 	TTL: defaults.TTL,
// 	body: {
// 		type: "ECHOREQUEST"
// 	}
// });

// console.log("n1 ping n2");

// internet.dns("n1").ping(internet.dns("n2").ip);

// console.log("n1 ping n3");

// internet.dns("n1").ping(internet.dns("n3").ip);

// console.log("");

// internet.dns("n1").trace(internet.dns("n2").ip);

// console.log("n1 trace n4");

// internet.dns("n1").traceroute(internet.dns("n3").ip);

// ARP_REQUEST|00:00:00:00:00:01,FF:FF:FF:FF:FF:FF|192.168.0.2,192.168.0.1
// ARP_REPLY|00:00:00:00:00:05,00:00:00:00:00:01|192.168.0.1,192.168.0.2
// ICMP_ECHOREQUEST|00:00:00:00:00:01,00:00:00:00:00:05|192.168.0.2,192.168.1.2|1
// ICMP_TIMEEXCEEDED|00:00:00:00:00:05,00:00:00:00:00:01|192.168.0.1,192.168.0.2|8
// ICMP_ECHOREQUEST|00:00:00:00:00:01,00:00:00:00:00:05|192.168.0.2,192.168.1.2|2
// ARP_REQUEST|00:00:00:00:00:06,FF:FF:FF:FF:FF:FF|192.168.1.1,192.168.1.2
// ARP_REPLY|00:00:00:00:00:03,00:00:00:00:00:06|192.168.1.2,192.168.1.1
// ICMP_ECHOREQUEST|00:00:00:00:00:06,00:00:00:00:00:03|192.168.0.2,192.168.1.2|1
// ICMP_ECHOREPLY|00:00:00:00:00:03,00:00:00:00:00:06|192.168.1.2,192.168.0.2|8
// ICMP_ECHOREPLY|00:00:00:00:00:05,00:00:00:00:00:01|192.168.1.2,192.168.0.2|7
