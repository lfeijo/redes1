'use strict';

var Inter = require("./inter");
var Node = require("./node");
var Router = require("./router");
var auxFuncs = require("./aux");

for (var property in auxFuncs) {
    if (auxFuncs.hasOwnProperty(property)) {
        global[property] = auxFuncs[property];
    }
}

class Internet {
	constructor() {
		this.nodes = {};
		this.inters = {};
	}
	addNode(name, ip, mask, gateway, mac) {
		var node = new Node(ip, mask, gateway, mac);
		// if subnet has inter instantiated
		var subnet = subnetAddress(ip, mask);
		if (subnet in this.inters) {
			// assign this node to that inter
			node.inter = this.inters[subnet];
			this.inters[subnet].nodes.push(node);
		} else {
			// new inter
			var i = new Inter();
			this.inters[subnet] = i;
			// assign node to new inter
			this.inters[subnet].nodes.push(node);
			node.inter = this.inters[subnet];
		}
		this.nodes[name] = node;
	}
	addRouter(name, interfaces) {
		var router = new Router();
		for (var i = 0; i < interfaces.length; i++) {
			var subnet = subnetAddress(interfaces[i].ip, interfaces[i].mask);
			if (subnet in this.inters) {
				router.addInterface(interfaces[i].ip, interfaces[i].mask, interfaces[i].mac, this.inters[subnet]);
				this.inters[subnet].nodes.push(router);
			} else {
				var it = new Inter();
				this.inters[subnet] = it;
				// assign this interface to the new intercommunicator
				router.addInterface(interfaces[i].ip, interfaces[i].mask, interfaces[i].mac, this.inters[subnet]);
				this.inters[subnet].nodes.push(router);
			}
		}
		this.nodes[name] = router;
	}
	dns(name) {
		return this.nodes[name];
	}

};

module.exports = Internet;