'use strict';

function printPkt(pkt) {
	if (pkt.type === "ARP") {
		console.log(pkt.type+"_"+pkt.body.operation+"|"+pkt.source_mac+","+pkt.dest_mac+"|"+pkt.body.sourceIp+","+pkt.body.targetIp);
	} else if (pkt.type === "IPv4") {
		if (pkt.body.protocol === "ICMP") {
			console.log(pkt.body.protocol+"_"+pkt.body.body.type+"|"+pkt.source_mac+","+pkt.dest_mac+"|"+pkt.body.sourceIp+","+pkt.body.destinationIp+"|"+pkt.body.TTL);
		}
	};
};

// Inter Communicator
// simula o meio fisico
// recebe um pacote ethernet e distribui pra todos os nodos da rede
// cada subnet deve ter um Inter proprio
class Inter {
	constructor() {
		this.nodes = [];
	}
	//
	// send packets to all nodes connected to this intercommunicator
	// pkt:
	// dest_mac
	// source_mac
	// type
	// body
	send(pkt){
		printPkt(pkt);
		for (var i = this.nodes.length - 1; i >= 0; i--) {
			this.nodes[i].comm(pkt);
		};
	}
};

module.exports = Inter;