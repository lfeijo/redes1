var funcs = {
	pad: function(str, max) {
		str = str.toString();
		return str.length < max ? this.pad("0" + str, max) : str;
	},
	and: function(A, B){
		var a = A.split(".");
		var b = B.split(".");
		var res = "";
		for (var i = 0; i < 4; i++) {
			var anded = "";
			for (var j = 0; j < 8; j++) {
				if (this.pad(Number(a[i]).toString(2),8)[j] === '1' &&
					this.pad(Number(b[i]).toString(2),8)[j] === '1'){
					anded += "1";
				} else {
					anded += "0";
				}
			}
			res += anded;
		};
		return res;
	},
	binaryToIp: function(bin) {
		var out = "";
		out += parseInt(bin.substring(0, 8), 2)+".";
		out += parseInt(bin.substring(8, 16), 2)+".";
		out += parseInt(bin.substring(16, 24), 2)+".";
		out += parseInt(bin.substring(24, 32), 2)+"";
		return out;
	},
	isSameNetwork: function(a_ip, b_ip, mask){
		return (this.and(a_ip, mask) === this.and(b_ip, mask));
	},
	prefixToMask: function(prefix) {
		var bin = "";
		for (var i = 0; i < parseInt(prefix); i++) {
			bin += "1";
		}
		for (var i = parseInt(prefix); i <32; i++) {
			bin += "0";
		};
		return this.binaryToIp(bin);
	},
	broadcast: function(ip, mask) {
		return false;
		// to be implemented
	},
	subnetAddress: function(ip, mask) {
		return this.binaryToIp(and(ip, mask));
	}
};

module.exports = funcs;