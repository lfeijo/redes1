'use strict';

var defaults = require("./defaults");
var auxFuncs = require("./aux");

for (var property in auxFuncs) {
    if (auxFuncs.hasOwnProperty(property)) {
        global[property] = auxFuncs[property];
    }
}

class Router {
	constructor() {
		this.interfaces = {};
		this.interfacePort = {};
		this.interfaceIp = {};
		this.rtrt = {};
		this.arpt = {};
	}
	//
	// each interface has a number (its position in the array) and it's chosen sequentially
	// 
	addInterface(ip, mask, mac, inter) {
		this.interfaces[mac] = {
			ip: ip,
			mac: mac,
			mask: mask,
			inter: inter,
			port: Object.keys(this.interfaces).length
		};
		this.interfaceIp[ip] = mac;
		this.interfacePort[Object.keys(this.interfaces).length-1] = mac;
		return Object.keys(this.interfaces).length-1;
	}
	//
	//
	//
	addTableEntry(destinationIp, mask, nextHop, port) {
		this.rtrt[destinationIp] = {
			mask: mask,
			nextHop: nextHop,
			port: port
		};
	}
	//
	//
	//			
	routeFor(ip) {
		for (var t_ip in this.rtrt) {
			if (this.rtrt.hasOwnProperty(t_ip)) {
				if (isSameNetwork(ip, t_ip, this.rtrt[t_ip].mask)) {
					var entry = this.rtrt[t_ip];
					entry.ip = t_ip;
					entry.interface = this.interfaces[this.interfacePort[entry.port]];
					return this.rtrt[t_ip];
				}
			}
		}
	}
	//
	// comm receives ethernet packages
	// all communication coming in the node is received by comm
	// Types: ARP, IPv4
	// pkt:
	// dest_mac
	// source_mac
	// type
	// body
	comm(pkt) {
		if (!(pkt.source_mac in this.interfaces)) { // this fixes a simulation bug in which
										 			// it sends packets to itself too
			if (pkt.dest_mac in this.interfaces || 	// if packet for this node
				pkt.dest_mac === "FF:FF:FF:FF:FF:FF") { // or packet is broadcast
				if (pkt.type==="ARP") {
					this.arp(pkt.body); // trims out ethernet headers
				} else if (pkt.type==="IPv4") {
					this.ipv4(pkt.body); // trims out ethernet headers
				}
			} else {
				// promiscuous mode would be done here
			}
		}
	}
	//
	// body:
	// operation (REQUEST, REPLY)
	// targetIp
	// targetMac
	// sourceIp
	// sourceMac
	arp(body) {
		if (body.targetIp in this.interfaceIp) {
			if (body.operation==="REQUEST") {
				this.arpt[body.sourceIp] = body.sourceMac;
				this.sendEthernet(this.routeFor(body.sourceIp).port, {
					dest_mac: body.sourceMac,
					source_mac: this.interfaceIp[body.targetIp],
					type: "ARP",
					body: {
						operation: "REPLY",
						sourceIp: body.targetIp,
						sourceMac: this.interfaceIp[body.targetIp],
						targetIp: body.sourceIp,
						targetMac: body.sourceMac
					}
				});
			} else if (body.operation==="REPLY") {
				this.arpt[body.sourceIp] = body.sourceMac;
			}
		}
	}
	//
	// body must contain:
	// protocol: ICMP
	// sourceIp, destinationIp
	// TTL
	//
	ipv4(ip_pkt) {
		if (ip_pkt.TTL>1) {
			ip_pkt.TTL -= 1;
			// nodes that arent routers ignore packets with destinationIps not for them
			if (ip_pkt.destinationIp in this.interfaceIp ||
				ip_pkt.destinationIp===broadcast(this.ip, this.mask)) { // broadcast method is gonna break
				if (ip_pkt.protocol==="ICMP") {
					this.icmp(ip_pkt);
				}
			} else { // route packet
				this.sendIpv4(ip_pkt);
			}
		} else {
			var route = this.routeFor(ip_pkt.sourceIp);
			this.sendIcmp(route.interface.ip, ip_pkt.sourceIp, defaults.TTL, "TIMEEXCEEDED");
		}
	}
	//
	// body must contain:
	// type: ECHO_REPLY, ECHO_REQUEST
	//
	icmp(ip_pkt) {
		if (ip_pkt.body.type==="ECHOREQUEST") {
			var route = this.routeFor(ip_pkt.sourceIp);
			this.sendIcmp(route.interface.ip, ip_pkt.sourceIp, defaults.TTL, "ECHOREPLY");
		} else if (ip_pkt.body.type==="ECHOREPLY") {

		} else if (ip_pkt.body.type==="TIMEEXCEEDED") {

		}
	}
	//
	// send packet from this node
	// pkt:
	// dest_mac
	// source_mac
	// type
	// body
	sendEthernet(port, pkt) {
		// console.log(port);
		// console.log(this.interfaces);
		// console.log(this.interfacePort);

		this.interfaces[this.interfacePort[port]].inter.send(pkt);
	}
	//
	// send IP packet from this node
	// pkt:
	// protocol
	// sourceIp
	// destinationIp
	// TTL
	// body
	sendIpv4(pkt) {
		var protocol = pkt.protocol,
			sourceIp = pkt.sourceIp,
			destinationIp = pkt.destinationIp,
			TTL = pkt.TTL,
			body = pkt.body;
		var route = this.routeFor(pkt.destinationIp);
		var targetInterface = this.interfaces[this.interfacePort[route.port]];
		var realDestinationIp;

		if (route.nextHop === "0.0.0.0") { // direct
			realDestinationIp = pkt.destinationIp;
		} else { // another router
			realDestinationIp = route.nextHop;
		}

		if (realDestinationIp in this.arpt) { // if realDestinationIp in arp table
			this.sendEthernet(targetInterface.port, {  // send to ethernet module
				dest_mac: this.arpt[realDestinationIp],
				source_mac: targetInterface.mac,
				type: "IPv4",
				body: {
					protocol: protocol,
					sourceIp: sourceIp,
					destinationIp: pkt.destinationIp,
					TTL: TTL,
					body: body
				}
			});
		} else { // if not in table
			// do arp request
			this.sendEthernet(targetInterface.port, {  // send to ethernet module
				dest_mac: "FF:FF:FF:FF:FF:FF",
				source_mac: targetInterface.mac,
				type: "ARP",
				body: {
					operation: "REQUEST",
					sourceIp: targetInterface.ip,
					sourceMac: targetInterface.mac,
					targetIp: realDestinationIp,
					targetMac: "00:00:00:00:00:00" 
				}
			});
			// 	wait for arp reply
			if (realDestinationIp in this.arpt) { //  if ip is now in arp table
				this.sendEthernet(targetInterface.port, {  // send to ethernet module
					dest_mac: this.arpt[realDestinationIp],
					source_mac: targetInterface.mac,
					type: "IPv4",
					body: {
						protocol: protocol,
						sourceIp: sourceIp,
						destinationIp: pkt.destinationIp,
						TTL: TTL,
						body: body
					}
				});
			}
		}
	}
	//
	//
	//
	sendIcmp(sourceIp, destinationIp, TTL, type) {
		this.sendIpv4({
			protocol: "ICMP",
			sourceIp: sourceIp,
			destinationIp: destinationIp,
			TTL: TTL,
			body: {
				type: type
			}
		});
	}
};

module.exports = Router;