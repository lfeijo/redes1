'use strict';

var defaults = require("./defaults");
var auxFuncs = require("./aux");

for (var property in auxFuncs) {
    if (auxFuncs.hasOwnProperty(property)) {
        global[property] = auxFuncs[property];
    }
}

class Node {
	constructor(ip, mask, gateway, mac) {
		this.ip = ip;
		this.mac = mac;
		this.mask = mask;
		this.gateway = gateway;
		this.arpt = {};
		this.inter = {};// reference to subnet's inter comm
		this.log = "";
	}
	//
	// comm receives ethernet packages
	// all communication coming in the node is received by comm
	// Types: ARP, IPv4
	// pkt:
	// dest_mac
	// source_mac
	// type
	// body
	comm(pkt) {
		if (pkt.source_mac!==this.mac) { // this fixes a simulation bug in which
										 // it sends packets to itself too
			if (pkt.dest_mac === this.mac || 	// if packet for this node
				pkt.dest_mac === "FF:FF:FF:FF:FF:FF") { // or packet is broadcast
				if (pkt.type==="ARP") {
					this.arp(pkt.body); // trims out ethernet headers
				} else if (pkt.type==="IPv4") {
					this.ipv4(pkt.body); // trims out ethernet headers
				}
			} else {
				// promiscuous mode would be done here
			}
		}
	}
	//
	// body:
	// operation (REQUEST, REPLY)
	// targetIp
	// targetMac
	// sourceIp
	// sourceMac
	arp(body) {
		if (body.targetIp===this.ip) {
			if (body.operation==="REQUEST") {
				this.arpt[body.sourceIp] = body.sourceMac;
				this.sendEthernet({
					dest_mac: body.sourceMac,
					source_mac: this.mac,
					type: "ARP",
					body: {
						operation: "REPLY",
						sourceIp: this.ip,
						sourceMac: this.mac,
						targetIp: body.sourceIp,
						targetMac: body.sourceMac
					}
				});
			} else if (body.operation==="REPLY") {
				this.arpt[body.sourceIp] = body.sourceMac;
			}
		}
	}
	//
	// body must contain:
	// protocol: ICMP
	// sourceIp, destinationIp
	// TTL
	//
	ipv4(ip_pkt) {
		if (ip_pkt.TTL>=1) {
			ip_pkt.TTL -= 1;
			// nodes that arent routers ignore packets with destinationIps not for them
			if (ip_pkt.destinationIp===this.ip ||
				ip_pkt.destinationIp===broadcast(this.ip, this.mask)) {
				if (ip_pkt.protocol==="ICMP") {
					this.icmp(ip_pkt);
				}
			} else {
				console.log("I'm node "+this.ip+", not a router!");
			}
		} else { // respond ICMP_TIMEEXCEEDED
			this.sendIcmp(this.ip, ip_pkt.sourceIp, defaults.TTL, "TIMEEXCEEDED");
		}
	}
	//
	// body must contain:
	// type: ECHO_REPLY, ECHO_REQUEST
	//
	icmp(ip_pkt) {
		if (ip_pkt.body.type==="ECHOREQUEST") {
			this.sendIcmp(this.ip, ip_pkt.sourceIp, defaults.TTL, "ECHOREPLY");
		} else if (ip_pkt.body.type==="ECHOREPLY") {
			this.log = "ECHOREPLY";
		} else if (ip_pkt.body.type==="TIMEEXCEEDED") {
			this.log = "TIMEEXCEEDED";
		}
	}
	//
	// send packet from this node
	// pkt:
	// dest_mac
	// source_mac
	// type
	// body
	sendEthernet(pkt) {
		this.inter.send(pkt);
	}
	//
	// send IP packet from this node
	// pkt:
	// protocol
	// sourceIp
	// destinationIp
	// TTL
	// body
	sendIpv4(pkt) {
		var protocol = pkt.protocol,
			sourceIp = pkt.sourceIp,
			destinationIp = pkt.destinationIp,
			TTL = pkt.TTL,
			body = pkt.body;
		var realDestinationIp;
		var mac;
		if (isSameNetwork(this.ip, destinationIp, this.mask)) { // if destinationIp in same subnet
			realDestinationIp = destinationIp; // send packet directly
		} else {
			realDestinationIp = this.gateway; // send packet to default gateway
		}

		if (realDestinationIp in this.arpt) { // if realDestinationIp in arp table
			this.sendEthernet({  // send to ethernet module
				dest_mac: this.arpt[realDestinationIp],
				source_mac: this.mac,
				type: "IPv4",
				body: {
					protocol: protocol,
					sourceIp: sourceIp,
					destinationIp: pkt.destinationIp,
					TTL: TTL,
					body: body
				}
			});
		} else { // if not in table
			// do arp request
			this.sendEthernet({  // send to ethernet module
				dest_mac: "FF:FF:FF:FF:FF:FF",
				source_mac: this.mac,
				type: "ARP",
				body: {
					operation: "REQUEST",
					sourceIp: this.ip,
					sourceMac: this.mac,
					targetIp: realDestinationIp,
					targetMac: "00:00:00:00:00:00" 
				}
			});
			// 	wait for arp reply
			if (realDestinationIp in this.arpt) { //  if ip is now in arp table
				this.sendEthernet({  // send to ethernet module
					dest_mac: this.arpt[realDestinationIp],
					source_mac: this.mac,
					type: "IPv4",
					body: {
						protocol: protocol,
						sourceIp: sourceIp,
						destinationIp: pkt.destinationIp,
						TTL: TTL,
						body: body
					}
				});
			} else {
				// error: invalid ip
				console.log("error: invalid realDestinationIp on sendIpv4 '"+realDestinationIp+"' resolved from destinationIp '"+destinationIp+"'.");
			}
		}
	}
	//
	//
	//
	sendIcmp(sourceIp, destinationIp, TTL, type) {
		this.sendIpv4({
			protocol: "ICMP",
			sourceIp: sourceIp,
			destinationIp: destinationIp,
			TTL: TTL,
			body: {
				type: type
			}
		});
	}
	//
	// do ping
	//
	ping(ip) {
		this.sendIcmp(this.ip, ip, defaults.TTL, "ECHOREQUEST");
	}
	//
	// do traceroute
	//
	traceroute(ip) {
		var ttl = 1;
		while(this.log !== "ECHOREPLY") {

			this.sendIcmp(this.ip, ip, ttl, "ECHOREQUEST");

			if (this.log === "TIMEEXCEEDED") {
				ttl += 1;
				continue;
			} else {
				return; // ERROR
			}
			
		}
	}
};

module.exports = Node;